package cz.muni.fi.xuhlarik.grammarManager.unitTest;

import cz.muni.fi.xuhlarik.grammarManager.FileType;
import cz.muni.fi.xuhlarik.grammarManager.dao.XmlDataRepo;
import cz.muni.fi.xuhlarik.grammarManager.service.FileService;
import cz.muni.fi.xuhlarik.grammarManager.service.RuleService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

/**
 * @author Jakub Uhlarik
 */
public class RuleServiceTest {

    @Mock
    private FileService fileService;

    @Mock
    private XmlDataRepo xmlDataRepo;
    private RuleService ruleService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        ruleService = new RuleService(xmlDataRepo, fileService);
    }


    @Test
    public void EmptyGet(){
        when(xmlDataRepo.getRuleByLeft("id")).thenReturn("");
        try {
            ruleService.getRuleByLeft("id", FileType.XML_SRGS);
        }catch (ResponseStatusException e){
            assertThat(e.getStatusCode()).isEqualTo( HttpStatus.NOT_FOUND);
            assertThat(e.getReason()).isEqualTo("rule with left side: id not found");
        }
    }

    @Test
    public void getJsgf(){
        String xml="<rule id='rule1'>xd<rule1>";
        String jsgf = "rule1 xd;";
        when(xmlDataRepo.getRuleByLeft("rule1")).thenReturn(xml);
        when(fileService.xmlToJSGF(eq(xml),anyBoolean())).thenReturn(jsgf);
        String result = ruleService.getRuleByLeft("rule1",FileType.JSGF);
        assertThat(result).isEqualTo(jsgf);

    }

    @Test
    public void getXml(){
        String xml="<rule id='rule1'>xd<rule1>";
        when(xmlDataRepo.getRuleByLeft("rule1")).thenReturn(xml);
        String result = ruleService.getRuleByLeft("rule1",FileType.XML_SRGS);
        assertThat(result).isEqualTo(xml);

    }
}
