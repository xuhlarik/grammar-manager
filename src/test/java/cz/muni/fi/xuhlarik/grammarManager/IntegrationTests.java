package cz.muni.fi.xuhlarik.grammarManager;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Jakub Uhlarik
 */

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class IntegrationTests {

    @Autowired
    private MockMvc mockMvc;
   private  static final String xmlProlog= "<?xml version='1.0'?>";

   private  final  static String rule = """
   <rule xmlns="http://www.w3.org/2001/06/grammar" id="p">
		<one-of>
			<item>d </item>
			<item>pred<ruleref uri="#xd"/>zad</item>
		</one-of>
	</rule>;""";

    private final static String testXml= """
                 <grammar xmlns="http://www.w3.org/2001/06/grammar" version="1.0" root="sde" xml:lang="en">
                	<rule id="p">
                		<one-of>
                			<item>d </item>
                			<item>pred<ruleref uri="#xd"/>zad</item>
                		</one-of>
                	</rule>
                	<rule id="sde">
                		<item> mam rad zvuk 
                			<ruleref uri="#mirozbirka"/>  zvuk jablono
                		</item>
                	</rule>
                	<!--comment -->
                </grammar>
              
                                """;


    @Test
    public void getAllUid() throws Exception {
        MvcResult uri=mockMvc.perform(post("/api/grammars").content(xmlProlog+testXml))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.TEXT_PLAIN)).andReturn();
        MvcResult result = mockMvc.perform(get("/api/grammars/uid"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andReturn();
        assertThat( result.getResponse().getContentAsString()).contains(uri.getResponse().getContentAsString());
    }

    @Test
    public void addGetUID() throws Exception {

        MvcResult result=mockMvc.perform(post("/api/grammars").content(xmlProlog+testXml))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.TEXT_PLAIN)).andReturn();
        String uid = result.getResponse().getContentAsString();
        MvcResult uids= mockMvc.perform(get("/api/grammars/uid"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON)).andReturn();
        assertThat( uids.getResponse().getContentAsString()).contains(uid);
        MvcResult grammar= mockMvc.perform(get("/api/grammars/{uid}",uid))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_XML)).andReturn();
        assertThat(grammar.getResponse().getContentAsString().replaceAll("\\W","")).isEqualTo(testXml.replaceAll("\\W",""));


    }

    private static final String testJSGF = """
            
            #JSGF V1.0;
            grammar a;
            
            import <com.xd>;
            <lul> = (x | d | f);
            """;//todo
    private static final String ruleJSGF="<lul> = (x | d | f)";

    @Test
    public void addGetJSGF() throws Exception {

        MvcResult result=mockMvc.perform(post("/api/grammars").content(testJSGF))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.TEXT_PLAIN)).andReturn();
        String uid = result.getResponse().getContentAsString();
        MvcResult uids= mockMvc.perform(get("/api/grammars/uid"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON)).andReturn();
        assertThat( uids.getResponse().getContentAsString()).contains(uid);
        MvcResult grammar= mockMvc.perform(get("/api/grammars/{uid}",uid).param("format","jsgf"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.TEXT_PLAIN)).andReturn();
        assertThat(grammar.getResponse().getContentAsString().replaceAll("\r\n","\n").trim()).isEqualTo(testJSGF.trim());
    }

    @Test
    public void  addMultiple() throws Exception {
        Files.list(Paths.get("src/test/java/cz/muni/fi/xuhlarik/grammarManager/resource/")).filter(Files::isRegularFile).forEach(file-> {
            try {
                Charset charset;
                if (file.endsWith("j5.xml")||file.endsWith("j2b.xml")||file.endsWith("j3b.xml")||file.endsWith("j4b.xml")){
                    charset= StandardCharsets.ISO_8859_1;
                }else{
                    charset= StandardCharsets.UTF_8;
                }
               String content = Files.readString(file,charset);
                mockMvc.perform(post("/api/grammars").content(content))
                        .andExpect(status().isCreated())
                        .andExpect(content().contentType(MediaType.TEXT_PLAIN));
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    @Test
    public void addGetRoot() throws Exception {
        mockMvc.perform(post("/api/grammars").content(xmlProlog+testXml))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.TEXT_PLAIN));
        MvcResult result=mockMvc.perform(get("/api/grammars/root/{root}","sde"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_XML)).andReturn();
        assertThat(result.getResponse().getContentAsString().replaceAll("\\W","")).contains(testXml.replaceAll("\\W",""));
    }

    @Test
    public void addGetName() throws Exception {
       mockMvc.perform(post("/api/grammars").content(testJSGF))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.TEXT_PLAIN));
        MvcResult result=mockMvc.perform(get("/api/grammars/name/{name}","a"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.TEXT_PLAIN)).andReturn();
        assertThat(result.getResponse().getContentAsString().replaceAll("\r\n","\n").trim()).contains(testJSGF.trim());
    }

    @Test
    public void addGetLeft() throws Exception {
        mockMvc.perform(post("/api/grammars").content(xmlProlog+testXml))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.TEXT_PLAIN));
        MvcResult result=mockMvc.perform(get("/api/rules/left/{root}","p"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_XML)).andReturn();
        assertThat(result.getResponse().getContentAsString().replaceAll("\\W","")).contains(rule.replaceAll("\\W",""));
    }
    @Test
    public void addGetLeftJSGF() throws Exception {
        mockMvc.perform(post("/api/grammars").content(testJSGF))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.TEXT_PLAIN));
        MvcResult result=mockMvc.perform(get("/api/rules/left/{left}","lul").param("format","jsgf"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.TEXT_PLAIN)).andReturn();
        assertThat(result.getResponse().getContentAsString().replaceAll("\r\n","\n").trim()).contains(ruleJSGF.trim());
    }





}
