package cz.muni.fi.xuhlarik.grammarManager.unitTest;

import cz.muni.fi.xuhlarik.grammarManager.FileType;
import cz.muni.fi.xuhlarik.grammarManager.dao.XmlDataRepo;
import cz.muni.fi.xuhlarik.grammarManager.service.FileService;
import cz.muni.fi.xuhlarik.grammarManager.service.GrammarService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

/**
 * @author Jakub Uhlarik
 */
public class GrammarServiceTest {

    @Mock
    private FileService fileService;

    @Mock
    private XmlDataRepo xmlDataRepo;
    private GrammarService grammarService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        grammarService = new GrammarService(xmlDataRepo, fileService);
    }

    @Test
    public void testEmptyRoot() {

        when(xmlDataRepo.getByRoot("empty")).thenReturn("");
        try {
            grammarService.getGrammarByRootSymbol("empty");
        } catch (ResponseStatusException e) {
            assertThat(e.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
            assertThat(e.getReason()).isEqualTo("grammar with root rule: empty not found");
        }
    }

    @Test
    public void testRoot() {
        String xml = "<grammar root='xml'></grammar>";
        when(xmlDataRepo.getByRoot("xml")).thenReturn(xml);
        String result = grammarService.getGrammarByRootSymbol("xml");
        assertThat(result).isEqualTo(xml);
    }

    @Test
    public void testEmptyName() {

        when(xmlDataRepo.getByJsgfName("name")).thenReturn("");
        try {
            grammarService.getGrammarByJsgfName("name");
        } catch (ResponseStatusException e) {
            assertThat(e.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
            assertThat(e.getReason()).isEqualTo("grammar with name: name not found");
        }
    }

    @Test
    public void testEmptyUri() {

        when(xmlDataRepo.getByUid("uid")).thenReturn("");
        try {
            grammarService.getGrammarByUid("uid", FileType.XML_SRGS);
        } catch (ResponseStatusException e) {
            assertThat(e.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
            assertThat(e.getReason()).isEqualTo("grammar with uid: uid not found");
        }
    }

    @Test
    public void testUriXml() {
        String xml = "<grammar r></grammar>";
        when(xmlDataRepo.getByUid("xml")).thenReturn(xml);
        String result = grammarService.getGrammarByUid("xml",FileType.XML_SRGS);
        assertThat(result).isEqualTo(xml);
    }

    @Test
    public void testUriJsgf() {
        String xml = "<grammar ></grammar>";
        String jsgf= "grammar xd;";
        when(xmlDataRepo.getByUid("jsgf")).thenReturn(xml);
        when(fileService.xmlToJSGF(eq(xml),anyBoolean())).thenReturn(jsgf);
        String result = grammarService.getGrammarByUid("jsgf",FileType.JSGF);
        assertThat(result).isEqualTo(jsgf);
    }

    @Test
    public void testName() {
        String xml = "<grammar ></grammar>";
        String jsgf= "grammar xd;";
        when(xmlDataRepo.getByJsgfName("jsgf")).thenReturn(xml);
        when(fileService.xmlToJSGF(eq(xml),anyBoolean())).thenReturn(jsgf);
        String result = grammarService.getGrammarByJsgfName("jsgf");
        assertThat(result).isEqualTo(jsgf);
    }

    @Test
    public void testAdd() {
        String uri = "1.xml";
        String xml="<grammar></grammar>";
        when(xmlDataRepo.add(xml)).thenReturn(uri);
        when(fileService.processValidateFile(eq(xml),any())).thenReturn(xml);
        String result = grammarService.addGrammarFromFile(xml,null);
        assertThat(result).isEqualTo(uri);
    }


}
