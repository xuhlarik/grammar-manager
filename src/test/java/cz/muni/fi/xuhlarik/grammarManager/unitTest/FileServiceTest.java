package cz.muni.fi.xuhlarik.grammarManager.unitTest;

import cz.muni.fi.xuhlarik.grammarManager.FileType;
import cz.muni.fi.xuhlarik.grammarManager.service.FileService;
import edu.cmu.sphinx.jsgf.JSGFGrammarException;
import edu.cmu.sphinx.jsgf.JSGFGrammarParseException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.web.server.ResponseStatusException;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * @author Jakub Uhlarik
 */
public class FileServiceTest {

    private FileService fileService;

    @BeforeEach
    public void setUp() throws SAXException {
        MockitoAnnotations.openMocks(this);
        fileService = new FileService();
    }

    private static String beginWithoutProlog(String lang) { return """
            <grammar version="1.0"
            xml:lang='"""+lang+"""
            '
            xmlns="http://www.w3.org/2001/06/grammar">
            <meta name="Source" content="j.jsgf"/>
            """;}

    private final String xmlEnd="</grammar>";

    private final String comment = "<!--Automaticaly generated xml-srgs grammar from jsgf-->";


    @Test
    public void xmlFormatUnknown() {
        String file1 = """
                <?xml version="1.0" ?><grammar></grammar>
                """;
        String file2 = """
                                
                                
                                
                                
                                
                                
                <?xml version="1.0" encoding="UTF-8"?><grammar></grammar>
                """;
        String file3 = """
                                
                                
                                
                                
                                
                                
                                    
                                    <?xml version="1.0" encoding="UTF-8"?><grammar></grammar>
                """;
        String file4 = """
                <?xml version="1.0" encoding="UTF-8" standalone='yes'?><grammar></grammar>
                    """;

        List<String> list = List.of(file1, file2, file3, file4);
        for (String s : list) {

            String result = fileService.processValidateFile(s, null);
            assertThat(result).isEqualTo(s);

        }


    }

    @Test
    public void jsgfFormatUnknown() {
        String file1 = """
                #JSGF V1.0; grammar j;
                  """;
        String file2 = """
                 
                 
                 
                 
                 
                 
                #JSGF V1.0 ISO8859-5; 
                grammar j;
                 """;
        String file3 = """
                                
                                
                                
                                
                                
                                
                                    
                                   #JSGF V1.0 JIS ja;grammar j;
                """;

        String file4 = """
                                
                                
                                
                                
                                
                                
                                    
                                   #JSGF V1.0 cz;grammar j;
                """;

        String file5 = """
                                
                                
                                
                                
                                
                                
                                    
                                   #JSGF V1.0 jis;grammar j;
                """;

        List<String> list = List.of(file1, file2, file3,file4,file5);
        int i=0;
        List<String> lang = List.of("","","ja","cz","jis");
        for (String s : list) {
            String result = fileService.processValidateFile(s, null);
            assertThat(result).isEqualTo(beginWithoutProlog(lang.get(i))+comment + "</grammar>");
            i++;
        }


    }

    @Test
    public void testJsgfSimpleExpConvert()  {
        String jsgf = """
                #JSGF V1.0 cz;
                grammar j;
                public <rule1> = <gruk>;
                <rule2> = g*;
                <rule3> = f+;
                <rule4> = [s];
                <rule5> = l|h;
                <rule6> = g g g;
                               
                """;
        String xml = beginWithoutProlog("cz") +comment+ """
                <rule id="rule5">
                <one-of><item>l</item><item>h</item></one-of>
                </rule>
                <rule id="rule4">
                <item repeat="0-1">s</item>
                </rule>
                <rule id="rule6">
                <item>g g g</item>
                </rule>
                <rule id="rule1" scope="public">
                <ruleref uri="#gruk"/>
                </rule>
                <rule id="rule3">
                <item repeat="1-">f</item>
                </rule>
                <rule id="rule2">
                <item repeat="0-">g</item>
                </rule>
                </grammar>
                                """;

        String result = fileService.jsgfToXml(jsgf);
        assertThat(result).isEqualTo(xml.trim());

    }
    @Test
    public void testWeightsToXml()  {
        String jsgf = """
                #JSGF V1.0 cz;
                grammar j;
                public <rule1> = /12.0/j|/9.0/h|/10.0/z;
                  
                """;

        String xml1 = beginWithoutProlog("cz") +comment+ """
                <rule id="rule1" scope="public">
                <one-of><item weight="12.0">j</item><item weight="9.0">h</item><item weight="10.0">z</item></one-of>
                </rule>
                """ + xmlEnd;

        String result = fileService.jsgfToXml(jsgf);
        assertThat(result).isEqualTo(xml1);

    }
    @Test
    public void testSpecialRules(){
        String jsgf = """
                #JSGF V1.0 cz;
                grammar j;
                public <rule1> = <NULL>|<VOID>;
                  
                """;

        String xml1 = beginWithoutProlog("cz") +comment+ """
                <rule id="rule1" scope="public">
                <one-of><item><ruleref special="NULL"/></item><item><ruleref special="VOID"/></item></one-of>
                </rule>
                """ + xmlEnd;

        String result = fileService.jsgfToXml(jsgf);
        assertThat(result).isEqualTo(xml1);
    }

    @Test
    public void testImportTagsRules(){
        String jsgf = """
                #JSGF V1.0 cz;
                grammar j;
                import <com.dd.dds>;
                public <rule1> = xd {lul};
                  
                """;

        String xml1 = beginWithoutProlog("cz") + """
                <meta name="seeAlso" content="com.dd.dds"/>
                """+comment+"""
                <rule id="rule1" scope="public">
                xd<tag>lul</tag>
                </rule>
                """ + xmlEnd;

        String result = fileService.jsgfToXml(jsgf);
        assertThat(result).isEqualTo(xml1);
    }

    @Test
    public void testJsgfHardExpConvert()  {
        String jsgf = """
                #JSGF V1.0;
                grammar j;
                public <rule1> = g*|p+|[l];
                  
                """;

        String xml1 =   beginWithoutProlog("")+comment+ """
                <rule id="rule1" scope="public">
                <one-of><item><item repeat="0-">g</item></item><item><item repeat="1-">p</item></item><item><item repeat="0-1">l</item></item></one-of>
                </rule>
                """ + xmlEnd;

        String result = fileService.jsgfToXml(jsgf);
        assertThat(result).isEqualTo(xml1);

    }

    @Test
    public void testJsgfHardConvert()  {
        String jsgf = """
                #JSGF V1.0 cz;
                grammar j;
                public <rule1> = g*|p+|[l];
                  
                """;

        String xml1 = beginWithoutProlog("cz") +comment+ """
                <rule id="rule1" scope="public">
                <one-of><item><item repeat="0-">g</item></item><item><item repeat="1-">p</item></item><item><item repeat="0-1">l</item></item></one-of>
                </rule>
                """ + xmlEnd;

        String result = fileService.jsgfToXml(jsgf);
        assertThat(result).isEqualTo(xml1);

    }

    @Test
    public void testBadFormatJSGF() {
        String jsgf = """
                #JSGF V1.0;
                grammar j
                public <rule1> = g*|p+|[l];
                  
                """;
        String jsgf1 = """
                #JSGF V1.0;
                public <rule1> = g*|p+|[l];
                  
                """;
        String jsgf2 = """
                #JSGF V1.0;
                grammar j;
                /*public <rule1> = g*|p+|[l];
                  
                """;
        String jsgf3 = """
                #JSGF;
                grammar j
                public <rule1> = g*|p+|[l];
                  
                """;
        List<String> jsgfList= List.of(jsgf,jsgf1,jsgf2,jsgf3);
        for (String jsgfIter:jsgfList){
            try {
                fileService.processValidateFile(jsgfIter, FileType.JSGF);
                assert false;
            }catch (ResponseStatusException e){
                assertThat(e.getMessage()).contains("jsgf parse");
            }
        }

    }

    @Test
    public void testXmltoJsgfTag() {
        String jsgf = """
                #JSGF V1.0 cz;
                grammar j;
                
                public <rule1> = (xd {luu});
                """;

        String xml1 = createSmallXml( """
                <rule scope="public" id="rule1">
                xd <tag>luu</tag>
                </rule>
                """ );

        String result = fileService.xmlToJSGF(xml1,true);
        assertThat(result).isEqualTo(jsgf);

    }

    private String createSmallXml(String content){
        return beginWithoutProlog("cz")+content+xmlEnd;
    }
    @Test
    public void testXmltoJsgfMore() {

        String jsgf = """
                #JSGF V1.0 cz;
                grammar j;
                
                <rule1> = xd;
                """;

        String xml = createSmallXml("""
                <rule id="rule1">
                xd
                </rule>
                """ );


        String jsgf1 = """
                #JSGF V1.0 cz;
                grammar j;
                
                public <rule1> = (xd | lul);
                """;

        String xml1 = createSmallXml("""
                <rule scope="public" id="rule1">
                <one-of><item>xd</item><item>lul</item></one-of>
                </rule>
                """ );


        String jsgf2 = """
                #JSGF V1.0 cz;
                grammar j;
                
                public <rule1> = (xd* | (l f)+ | [g] | k??);
                """;

        String xml2 = createSmallXml("""
                <rule scope="public" id="rule1">
                <one-of><item repeat="0-">xd</item>
                <item repeat="1-">l f</item>
                <item repeat="0-1">g</item>
                <item repeat="1-7">k</item>
                </one-of>
                </rule>
                """ );


        String jsgf3 = """
                #JSGF V1.0 cz;
                grammar j;
                
                public <rule1> = (<dg> | <VOID> | <NULL> | <GARBAGE>);
                """;

        String xml3 = createSmallXml("""
                <rule scope="public" id="rule1">
                <one-of><item><ruleref uri="#dg"/> </item>
              <item><ruleref special="VOID"/> </item>
              <item><ruleref special="NULL"/> </item>
              <item><ruleref special="GARBAGE"/> </item>
              </one-of>
                </rule>
                """ );

        String jsgf4 = """
                #JSGF V1.0 cz;
                grammar j;
                
                import <com.lul>;
                import <com.new>;        
                public <rule1> = (<dg> | <VOID>);
                """;

        String xml4 = createSmallXml("""
                <meta name="seeAlso" content="com.lul"/>
                <meta name="seeAlso" content="com.new"/>
                """+comment+"""
                <rule scope="public" id="rule1">
                <one-of><item><ruleref uri="#dg"/> </item>
                <item><ruleref special="VOID"/> </item>
              </one-of>
                </rule>
                """ );



        List<String> jsgfList= List.of(jsgf,jsgf1,jsgf2,jsgf3,jsgf4);
        List<String> xmlList = List.of(xml,xml1,xml2,xml3,xml4);
        for (int i=0;i<xmlList.size();i++){
            String result = fileService.xmlToJSGF(xmlList.get(i),true);
            assertThat(result).isEqualTo(jsgfList.get(i));
        }


    }
    @Test
    public void testJSGFRUleChange(){

        String xml ="""
                <rule scope="public" id="rule1">
                <one-of><item><ruleref uri="#dg"/> </item>
              <item><ruleref special="VOID"/> </item>
              <item><ruleref special="NULL"/> </item>
              <item><ruleref special="GARBAGE"/> </item>
              </one-of>
                </rule>
                """;
        String jsgf = """
               
                 public <rule1> = (<dg> | <VOID> | <NULL> | <GARBAGE>);
                """;

        String result = fileService.xmlToJSGF(xml,false);
        assertThat(result.trim()).isEqualTo(jsgf.trim());
    }


    @Test
    public void testEncapsulationHard(){
        String jsgf = """
                #JSGF V1.0 cz;
                grammar j;
                
                <f> = [((a b c d e f)* | la)+];
                """;
        String xml =createSmallXml(comment+ """
                <rule id="f">
                <item repeat="0-1"><item repeat="1-"><one-of><item><item repeat="0-"><item>a b c d e f</item></item></item><item>la</item></one-of></item></item>
                </rule>
                """);
        assertThat(fileService.jsgfToXml(jsgf)).isEqualTo(xml);
        assertThat(fileService.xmlToJSGF(fileService.jsgfToXml(jsgf),true)).isEqualTo(jsgf);
    }


}
