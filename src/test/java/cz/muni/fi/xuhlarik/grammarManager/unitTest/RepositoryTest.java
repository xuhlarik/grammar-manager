package cz.muni.fi.xuhlarik.grammarManager.unitTest;

import cz.muni.fi.xuhlarik.grammarManager.dao.XmlDataRepo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.server.ResponseStatusException;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Jakub Uhlarik
 */
@SpringBootTest
@ActiveProfiles("test")
public class RepositoryTest {

    private final XmlDataRepo xmlDataRepo;

    private final static String testXml= """
                 <grammar xmlns="http://www.w3.org/2001/06/grammar" version="1.0" xml:base="xd" root="sde" xml:lang="en">
                	<rule id="p">
                		<one-of>
                			<item>d
                </item>
                			<item>pred
                				<ruleref uri="#xd"/>zad
                			</item>
                		</one-of>
                	</rule>
                	<rule id="sde">
                		<item> mam rad zvuk 
                			<ruleref uri="#mirozbirka"/>  zvuk jabloní
                		</item>
                	</rule>
                	<!--comment -->
                </grammar>
             
                                """;

    private final static String testConvertedJSGF= """
                 <grammar xmlns="http://www.w3.org/2001/06/grammar" version="1.0" root="sde" xml:lang="en">
                    <meta name="Source" content="xd.jsgf"/>
                	<rule id="p">
                		<one-of>
                			<item>d
                </item>
                			<item>pred
                				<ruleref uri="#xd"/>zad
                			</item>
                		</one-of>
                	</rule>
                	<rule id="sde">
                		<item> mam rad zvuk 
                			<ruleref uri="#mirozbirka"/>  zvuk jabloní
                		</item>
                	</rule>
                	<!--comment -->
                </grammar>
             
                                """;

    private final static String testUnvalidXml2= """
                 <?xml version='1.0'?>
                 <grammar>
                	
                </grammar>
              
                                """;

    private final static String RuleSDe= """
                <rule xmlns="http://www.w3.org/2001/06/grammar" id="sde">
                		<item> mam rad zvuk 
                			<ruleref uri="#mirozbirka"/>  zvuk jabloní
                		</item>
                	</rule>
              
                                """;

    @Autowired
    private RepositoryTest(XmlDataRepo xmlDataRepo) {
        this.xmlDataRepo = xmlDataRepo;
    }


    @Test
    public void testAdd() {
        String uid=xmlDataRepo.add(testXml);
        xmlDataRepo.delete(uid);
    }


    @Test
    public void testGetUID() {
        String uid = xmlDataRepo.add(testXml.trim());
        assertThat(xmlDataRepo.getByUid(uid).replaceAll("\\W","")).isEqualTo(testXml.replaceAll("\\W",""));
        xmlDataRepo.delete(uid);
    }

    @Test
    public void testGetName() {
        String uid = xmlDataRepo.add(testConvertedJSGF.trim());
        assertThat(xmlDataRepo.getByJsgfName("xd").replaceAll("\\W","")).isEqualTo(testConvertedJSGF.replaceAll("\\W",""));
        xmlDataRepo.delete(uid);
    }

    @Test
    public void testGetRoot() {
        String uid = xmlDataRepo.add(testXml.trim());
        assertThat(xmlDataRepo.getByRoot("sde").replaceAll("\\W","")).contains(testXml.replaceAll("\\W",""));
        xmlDataRepo.delete(uid);
    }

    @Test
    public void testGetRuleLeft() {
        String uid = xmlDataRepo.add(testXml.trim());
        assertThat(xmlDataRepo.getRuleByLeft("sde").replaceAll("\\W","")).contains(RuleSDe.replaceAll("\\W",""));
        xmlDataRepo.delete(uid);
    }

    @Test
    public void testUnvalidXml() {
        try {
           xmlDataRepo.add(testUnvalidXml2.trim());
        }catch (ResponseStatusException e){
            assertThat(e.getMessage()).contains("valid");
        }
    }

}
