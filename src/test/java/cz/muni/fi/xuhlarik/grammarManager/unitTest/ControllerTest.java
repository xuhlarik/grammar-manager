package cz.muni.fi.xuhlarik.grammarManager.unitTest;


import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import cz.muni.fi.xuhlarik.grammarManager.FileType;
import cz.muni.fi.xuhlarik.grammarManager.api.GrammarController;
import cz.muni.fi.xuhlarik.grammarManager.api.RuleController;
import cz.muni.fi.xuhlarik.grammarManager.service.FileService;
import cz.muni.fi.xuhlarik.grammarManager.service.GrammarService;
import cz.muni.fi.xuhlarik.grammarManager.service.RuleService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.server.ResponseStatusException;

/**
 * @author Jakub Uhlarik
 */


public class ControllerTest {

    @Mock
    private RuleService ruleService;

    @Mock
    private GrammarService grammarService;

    @Mock
    private FileService fileService;

    private GrammarController grammarController;
    private RuleController ruleController;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        grammarController = new GrammarController(grammarService, fileService);
        ruleController = new RuleController(ruleService);
    }

    @Test
    public void testGrammarFromFileXmlGrammar() {
        String fileContent = "<grammar></grammar>";
        when(grammarService.addGrammarFromFile(fileContent, FileType.XML_SRGS)).thenReturn("uri");
        ResponseEntity<String> response = grammarController.grammarFromFile(fileContent, "xml");
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(response.getBody()).isEqualTo("uri");
    }

    @Test
    public void testGrammarFromFileUnknownGrammar() {
        String fileContent = "<grammar></grammar>";
        when(grammarService.addGrammarFromFile(fileContent, null)).thenReturn("uri");
        ResponseEntity<String> response = grammarController.grammarFromFile(fileContent, "u");
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(response.getBody()).isEqualTo("uri");
    }

    @Test
    public void testGrammarFromFileNotGivenGrammar() {
        String fileContent = "<grammar></grammar>";
        when(grammarService.addGrammarFromFile(fileContent, null)).thenReturn("uri");
        ResponseEntity<String> response = grammarController.grammarFromFile(fileContent, null);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(response.getBody()).isEqualTo("uri");
    }

    @Test
    public void testGrammarFromFileJsgfGrammar() {
        String fileContent = "grammar test; public <rule> = test;";
        when(grammarService.addGrammarFromFile(fileContent, FileType.JSGF)).thenReturn("uri");
        ResponseEntity<String> response = grammarController.grammarFromFile(fileContent, "jsgf");
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(response.getBody()).isEqualTo("uri");
    }

    @Test
    public void testGrammarFromFileInvalidFormat() {
        String fileContent = "grammar test; public <rule> = test;";
        try {
            grammarController.grammarFromFile(fileContent, "invalid-format");
        } catch (ResponseStatusException e) {

            assertThat(e.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
            assertThat(e.getReason()).isEqualTo("invalid file format");
        }
    }

    @Test
    public void testGetGrammarXmlByRoot() {
        String xml = "<grammar></grammar>";
        when(grammarService.getGrammarByRootSymbol(anyString())).thenReturn(xml);
        ResponseEntity<String> response = grammarController.getGrammarXmlByRoot("root");
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEqualTo(xml);
        assertThat(response.getHeaders().getContentType()).isEqualTo(MediaType.APPLICATION_XML);
    }

    @Test
    public void testGetGrammarByJsgfName() {
        String jsgf = "grammar test; public <rule> = test;";
        when(grammarService.getGrammarByJsgfName(anyString())).thenReturn(jsgf);
        ResponseEntity<String> response = grammarController.getGrammarByJsgfName("name");
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEqualTo(jsgf);
        assertThat(response.getHeaders().getContentType()).isEqualTo(MediaType.TEXT_PLAIN);
    }

    @Test
    public void testGetGrammarByUriXml() {
        String xml = "<grammar></grammar>";
        when(grammarService.getGrammarByUid("uri", FileType.XML_SRGS)).thenReturn(xml);
        ResponseEntity<String> response = grammarController.getGrammar("uri", "xml");
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEqualTo(xml);
        assertThat(response.getHeaders().getContentType()).isEqualTo(MediaType.APPLICATION_XML);
    }

    @Test
    public void testGetAllUris() {

        String uids="[uid1,uid2]";
        when(grammarService.getAllUid()).thenReturn(uids);
        ResponseEntity<String> response = grammarController.getAllUid();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEqualTo(uids);
        assertThat(response.getHeaders().getContentType()).isEqualTo(MediaType.APPLICATION_JSON);
    }

    @Test
    public void testGetGrammarByUriJsgf() {
        String jsgf = "grammar test; public <rule> = test;";
        when(grammarService.getGrammarByUid("uri", FileType.JSGF)).thenReturn(jsgf);
        ResponseEntity<String> response = grammarController.getGrammar("uri", "jsgf");
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEqualTo(jsgf);
        assertThat(response.getHeaders().getContentType()).isEqualTo(MediaType.TEXT_PLAIN);
    }

    @Test
    public void testGetGrammarByUriEmptyFormat() {
        try {
            grammarController.getGrammar("uri", "null");
        } catch (ResponseStatusException e) {
            assertThat(e.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
            assertThat(e.getReason()).isEqualTo("invalid file format");
        }


    }

    @Test
    public void testGetRuleByLeftEmpty() {
        try {
            ruleController.getRuleByLeft("left", "null");
        } catch (ResponseStatusException e) {
            assertThat(e.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
            assertThat(e.getReason()).isEqualTo("invalid file format");
        }
    }

    @Test
    public void testGetRuleByLeftXMl() {
        String xml = "<rule></rule>";
        when(ruleService.getRuleByLeft("left",FileType.XML_SRGS)).thenReturn(xml);
        ResponseEntity<String> response = ruleController.getRuleByLeft("left","xml");
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEqualTo(xml);
        assertThat(response.getHeaders().getContentType()).isEqualTo(MediaType.APPLICATION_XML);
    }

    @Test
    public void testGetRuleByLeftJsgf() {
        String jsgf = "public <rule> = test;";
        when(ruleService.getRuleByLeft("left",FileType.JSGF)).thenReturn(jsgf);
        ResponseEntity<String> response = ruleController.getRuleByLeft("left","jsgf");
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEqualTo(jsgf);
        assertThat(response.getHeaders().getContentType()).isEqualTo(MediaType.TEXT_PLAIN);
    }

}
