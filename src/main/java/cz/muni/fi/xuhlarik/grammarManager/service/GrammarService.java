package cz.muni.fi.xuhlarik.grammarManager.service;


import cz.muni.fi.xuhlarik.grammarManager.FileType;
import cz.muni.fi.xuhlarik.grammarManager.dao.XmlDataRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;


/**
 * @author Jakub Uhlarik
 */
@Service
public class GrammarService {

    private final XmlDataRepo xmlDataRepo;
    private final FileService fileService;


    @Autowired
    public GrammarService(
            XmlDataRepo xmlDataRepo, FileService fileService) {
        this.xmlDataRepo = xmlDataRepo;
        this.fileService = fileService;
    }

    /**
     * find grammar with root symbol
     *
     * @param rootSymbol root symbol of grammar
     * @return found grammar
     */
    public String getGrammarByRootSymbol(String rootSymbol) {
        String result = xmlDataRepo.getByRoot(rootSymbol);
        if (result.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "grammar with root rule: " + rootSymbol + " not found");
        }
        return result;
    }

    public String getAllUid() {
        return xmlDataRepo.allUid();
    }


    public String getGrammarByUid(String uid, FileType typeOfFile) {
        String result = xmlDataRepo.getByUid(uid);
        if (result.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "grammar with uid: " + uid + " not found");
        }
        if (typeOfFile == FileType.JSGF) {
            return fileService.xmlToJSGF(result, true);
        }
        return result;

    }

    public String getGrammarByJsgfName(String name) {
        String jsgfGrammar = xmlDataRepo.getByJsgfName(name);
        if (jsgfGrammar.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "grammar with name: " + name + " not found");
        }

        return fileService.xmlToJSGF(jsgfGrammar, true);
    }

    public String addGrammarFromFile(String file, FileType fileType) {
        String grammar = fileService.processValidateFile(file, fileType);
        return xmlDataRepo.add(grammar);
    }


}
