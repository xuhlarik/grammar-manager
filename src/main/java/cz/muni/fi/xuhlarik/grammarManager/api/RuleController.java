package cz.muni.fi.xuhlarik.grammarManager.api;

import cz.muni.fi.xuhlarik.grammarManager.FileType;
import cz.muni.fi.xuhlarik.grammarManager.service.RuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

/**
 * class pf function that are mapped to requests starting with /api/rules
 *
 * @author Jakub Uhlarik
 */

@RestController
@CrossOrigin
@RequestMapping(path = "api/rules")
public class RuleController {

    private final RuleService ruleService;

    @Autowired
    public RuleController(RuleService ruleService) {
        this.ruleService = ruleService;
    }

    /**
     * @param left   left symbol of rule
     * @param format requested format of rule
     * @return response entity with rules as body
     */
    @GetMapping("/left/{left}")
    public ResponseEntity<String> getRuleByLeft(@PathVariable String left, @RequestParam(defaultValue = "xml") String format) {
        FileType typeOfFile;
        switch (format.toLowerCase()) {
            case "xml" -> typeOfFile = FileType.XML_SRGS;
            case "jsgf" -> typeOfFile = FileType.JSGF;
            default -> throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "invalid file format");
        }
        String result = ruleService.getRuleByLeft(left, typeOfFile);
        return ResponseEntity.ok().contentType(typeOfFile == FileType.XML_SRGS ? MediaType.APPLICATION_XML : MediaType.TEXT_PLAIN).body(result);

    }

}
