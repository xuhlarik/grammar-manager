package cz.muni.fi.xuhlarik.grammarManager.service;

import cz.muni.fi.xuhlarik.grammarManager.FileType;
import edu.cmu.sphinx.jsgf.JSGFGrammarParseException;
import edu.cmu.sphinx.jsgf.JSGFRuleGrammar;
import edu.cmu.sphinx.jsgf.JSGFRuleGrammarFactory;
import edu.cmu.sphinx.jsgf.JSGFRuleGrammarManager;
import edu.cmu.sphinx.jsgf.parser.JSGFParser;
import edu.cmu.sphinx.jsgf.parser.TokenMgrError;
import edu.cmu.sphinx.jsgf.rule.JSGFRule;
import edu.cmu.sphinx.jsgf.rule.JSGFRuleName;
import org.jvoicexml.processor.grammar.Rule;
import org.jvoicexml.processor.srgs.xml.SrgsRuleGrammarParser;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class with functions to process files.
 *
 * @author Jakub Uhlarik
 */
@Service
public class FileService {


    private static final String VALIDITY_SCHEMA_NAMESPACE = "grammar.xsd";
    private static final String VALIDITY_SCHEMA_NO_NAMESPACE = "grammar-core.xsd";

    private final Validator nameSpaceValidator;
    private final Validator noNamespaceValidator;


    /**
     * class loads validators on start of program
     *
     * @throws SAXException when schema cant be loaded
     */
    public FileService() throws SAXException {
        SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schemaNamespace = factory.newSchema(new File("./xsd_schema/" + VALIDITY_SCHEMA_NAMESPACE));
        Schema schemaNoNamespace = factory.newSchema(new File("./xsd_schema/" + VALIDITY_SCHEMA_NO_NAMESPACE));
        nameSpaceValidator = schemaNamespace.newValidator();
        noNamespaceValidator = schemaNoNamespace.newValidator();
    }

    /**
     * main function for file processing and validation
     *
     * @param file     file to be processed
     * @param fileType file type null means unknown
     * @return processed file
     */
    public String processValidateFile(String file, FileType fileType) {
        if (fileType == null) {
            fileType = getFileExtension(file);
        }
        if (fileType == FileType.JSGF) {
            file = jsgfToXml(file);
        }
        validateXML(file);
        return file;

    }


    /**
     * function validates xml with schemas
     *
     * @param xml file to be validated
     */
    private void validateXML(String xml) {

        try {
            nameSpaceValidator.validate(new StreamSource(new StringReader(xml)));
        } catch (SAXException e) {
            if (e.getMessage().contains("Cannot find the declaration of element 'grammar'")) {
                try {
                    noNamespaceValidator.validate(new StreamSource(new StringReader(xml)));
                } catch (IOException ex) {
                    throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "error in parsing");
                } catch (SAXException ex) {
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "grammar not valid " + e.getMessage());
                }
                return;
            }
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "grammar not valid " + e.getMessage());
        } catch (IOException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "error in parsing");
        }


    }


    /**
     * function finds format from file
     *
     * @param file file from which is extracted format
     * @return Filetype of file
     */
    private FileType getFileExtension(String file) {

        Pattern jsgf = Pattern.compile("^\\W*#JSGF.*;");
        Matcher mJsgf = jsgf.matcher(file);
        if (mJsgf.find()) {
            return FileType.JSGF;
        }
        Pattern xml = Pattern.compile("^\\W*<\\?xml.*\\?>");
        Matcher mXml = xml.matcher(file);
        if (mXml.find()) {
            return FileType.XML_SRGS;
        }


        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "format cant be found from file");
    }

    /**
     * function converts xml file to jsgf
     *
     * @param xml     xml file to convert to jsgf
     * @param grammar if true  grammar, if false rule
     * @return jsgf grammar from xml file
     */
    public String xmlToJSGF(String xml, boolean grammar) {

        StringBuilder xmlString = new StringBuilder();
        SrgsRuleGrammarParser parser = new SrgsRuleGrammarParser();
        if (!grammar) {
            writeHeaderXml(xmlString, null);
        }
        xmlString.append(xml);
        if (!grammar) {
            xmlString.append("</grammar>");
        }

        List<Rule> rules;
        try {
            rules = parser.load(new StringReader(xmlString.toString()));

        } catch (URISyntaxException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "error in converting xml to jsgf");
        }

        StringBuilder jsgfString = new StringBuilder();
        if (grammar) {
            Map<String, Object> parserAttributes = parser.getAttributes();
            writeHeaderJSGF(jsgfString, (String) parserAttributes.get("name"), (String) parserAttributes.get("xml:lang"));
            Object imports = parser.getAttributes().get("imports");
            if (imports != null) {
                jsgfString.append(imports);
            }
        }

        rules.forEach(p -> jsgfString.append(p.toStringJSGF()).append("\n"));
        return jsgfString.toString();
    }

    /**
     * function finds language definition from grammar header
     *
     * @param jsgf jsgf grammar
     * @return language from jsgf grammar
     */
    private String getLangFromJSGF(String jsgf) {
        //finding lang
        Pattern pattern = Pattern.compile("\\W*#JSGF\\s*V\\d+(?:\\.\\d+)?(?:\\s+\\S+)?\\s+((?:[a-z]{2,3}|x|i)(?:-[a-zA-Z]{1,8})*);");
        Matcher matcher = pattern.matcher(jsgf);
        if (matcher.find()) {
            String lang = matcher.group(1);
            if (!lang.isEmpty()) {
                return lang;
            }

        }
        return null;
    }

    /**
     * function converts jsgf rule to xml
     *
     * @param file jsgf grammar to convert
     * @return xml file from jsgf grammar
     */
    public String jsgfToXml(String file) {
        JSGFRuleGrammar grammarFromJSGF;
        try {
            grammarFromJSGF = JSGFParser.newGrammarFromJSGF(new StringReader(file), new JSGFRuleGrammarFactory(new JSGFRuleGrammarManager()));
        } catch (JSGFGrammarParseException | TokenMgrError e) {
            if (e instanceof JSGFGrammarParseException jsgfGrammarParseException) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "jsgf parse error\n" + jsgfGrammarParseException.details);
            }
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "jsgf parse error\n" + e.getMessage(), e);
        }
        StringBuilder sb = new StringBuilder();
        String language = getLangFromJSGF(file);

        writeHeaderXml(sb, language);
        sb.append("<meta name=\"Source\" content=\"").append(grammarFromJSGF.getName()).append(".jsgf\"/>\n");
        for (JSGFRuleName importName : grammarFromJSGF.getImports()) {
            sb.append("<meta name=\"seeAlso\" content=\"").append(importName.getRuleName()).append("\"/>\n");
        }
        sb.append("<!--Automatically generated xml-srgs grammar from jsgf-->");


        for (String ruleName : grammarFromJSGF.getRuleNames()) {
            JSGFRule rule = grammarFromJSGF.getRule(ruleName);
            boolean publicRule = grammarFromJSGF.isRulePublic(ruleName);
            writeXmlRule(sb, publicRule, ruleName, rule.toStringXml());
        }
        sb.append("</grammar>");

        return sb.toString();
    }

    /**
     * function writes
     *
     * @param sb         string builder that contains grammar
     * @param publicRule symbolizes if rule is public
     * @param name       name of rule
     * @param rightSide  right side of rule
     */

    private void writeXmlRule(StringBuilder sb, boolean publicRule, String name, String rightSide) {
        sb.append("<rule ");
        sb.append("id=\"");
        sb.append(name);
        sb.append("\"");
        if (publicRule) {
            sb.append(" scope=\"public\"");
        }
        sb.append(">\n");
        sb.append(rightSide);
        sb.append("\n</rule>\n");
    }


    /**
     * function writes begin of  element grammar in XML-SRGS
     *
     * @param sb       string builder with
     * @param language language of grammar
     */
    private void writeHeaderXml(StringBuilder sb, String language) {

        if (language == null) {
            language = "";
        }


        sb.append("<grammar version=\"1.0\"\n");
        sb.append("xml:lang='").append(language).append("'\n");
        sb.append("xmlns=\"http://www.w3.org/2001/06/grammar\">\n");

    }

    /**
     * Function writes header to new JSGF grammars
     *
     * @param sb       string builder with grammar
     * @param name     name of JSGF grammar
     * @param language language of grammar
     */
    private void writeHeaderJSGF(StringBuilder sb, String name, String language) {
        sb.append("#JSGF V1.0").append((language == null || language.isEmpty()) ? "" : " " + language).append(";\n");
        sb.append("grammar ").append(name == null ? "defaulf" : name).append(";\n\n");
    }

}

