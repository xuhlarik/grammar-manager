package cz.muni.fi.xuhlarik.grammarManager.config;

import org.basex.BaseXServer;
import org.basex.api.client.ClientSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.env.Environment;


import java.io.IOException;



/**
 * class that starts basex server and client connection
 * @author Jakub Uhlarik
 */
@Configuration
public class XmlDataConfig{

    @Autowired
    private Environment environment;

    @Bean(destroyMethod = "close")
    @DependsOn("baseXServer")
    public ClientSession clientSession() throws IOException {
        String password = environment.getProperty("DATABASE_PASSWORD");
        String host = environment.getProperty("DATABASE_HOST");
        if (password == null) {
            password = "admin";
        }
        if (host == null) {
            host = "localhost";

        }
        System.out.println("\n\nConnecting to database at: " + host + "\n");
        return new ClientSession(host, 1984, "admin", password);

    }

    @Bean(destroyMethod = "stop")
    public BaseXServer baseXServer() throws IOException {
        //server exist exists
        if (BaseXServer.ping("localhost",1984)){
            return null;
        }
        String password = environment.getProperty("DATABASE_PASSWORD");
        String host = environment.getProperty("DATABASE_HOST");
        //docker host
        if (host!=null){
            return null;
        }

        if (password == null) {
            password = "admin";
        }
        return new BaseXServer("-P" + password);
    }



}
