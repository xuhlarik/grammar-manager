package cz.muni.fi.xuhlarik.grammarManager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication()
public class GrammarManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(GrammarManagerApplication.class, args);

    }


}
