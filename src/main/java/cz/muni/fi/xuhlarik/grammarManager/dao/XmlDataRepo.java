package cz.muni.fi.xuhlarik.grammarManager.dao;

import org.basex.api.client.ClientSession;
import org.basex.api.client.Query;
import org.basex.core.Command;
import org.basex.core.cmd.Add;
import org.basex.core.cmd.Check;
import org.basex.core.cmd.Delete;
import org.basex.core.cmd.XQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.util.UUID;

/**
 * @author Jakub Uhlarik
 */

@Repository
@Profile("!test")
public class XmlDataRepo {

    private final ClientSession session;
    private final String dbName;


    private static final String XQUERY_RULE_BY_LEFT = "/*:grammar/*:rule[(@id =$param1)]";


    private static final String XQUERY_GRAMMAR_BY_ROOT = "/*:grammar[(@root = $param1)]";


    public XmlDataRepo(ClientSession session, String dbName) throws IOException {
        this.dbName = dbName;
        this.session = session;
        session.execute(new Check(dbName));
    }

    @Autowired
    public XmlDataRepo(ClientSession session) throws IOException {
        this(session, "grammarsDB");
    }

    /**
     * function for executing commands
     *
     * @param command command to be executed
     * @return result of command
     */
    private String executeCommand(Command command) {
        try {
            return session.execute(command);
        } catch (IOException e) {
            if (e.toString().contains("lock file")) {
                //error when using BaseX gui
                throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "error no concurrent file add\n close database on different app\n ");
            }
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.toString());
        }
    }

    /**
     * function with usage of parametrized query
     *
     * @param XQuery xquery with string $param1 as parameter
     * @param param  parameter to be bind
     * @return result of xquery
     */
    private String executeParamXQuery(String XQuery, String param) {
        try {
            //xquery syntax in request
            Query cq = session.query("declare variable $param1 external; " + XQuery);
            cq.bind("param1", param, "");
            return cq.execute();
        } catch (IOException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.toString());
        }
    }

    /**
     * function finds grammar with uid
     *
     * @param uid uid of grammar
     * @return grammar with uid
     */
    public String getByUid(String uid) {
        return executeParamXQuery("db:open('" + dbName + "',$param1)", uid);
    }

    /**
     * function finds grammars with root symbol
     *
     * @param root root symbol of grammar
     * @return grammars with root symbol
     */
    public String getByRoot(String root) {
        return executeParamXQuery(XQUERY_GRAMMAR_BY_ROOT, root);
    }

    /**
     * function finds rules with left symbol
     *
     * @param left left symbol
     * @return rules with left symbol
     */
    public String getRuleByLeft(String left) {
        return executeParamXQuery(XQUERY_RULE_BY_LEFT, left);
    }


    /**
     * function adds grammar to database and sets its uuid
     *
     * @param xml file to be added
     * @return uuid of added grammar
     */
    public String add(String xml) {
        UUID uuid = UUID.randomUUID();
        executeCommand(new Add(uuid.toString(), xml));
        return uuid.toString();
    }

    /**
     * function returns list of uids used in database
     *
     * @return list of all uids in json format
     */

    public String allUid() {
        return executeCommand(new XQuery("array{db:list(\"" + dbName + "\")}"));
    }

    //only test usage
    public void delete(String uid) {
        executeCommand(new Delete(uid));
    }

    /**
     * function find grammar with jsgf name
     *
     * @param name name of jsgf grammar
     * @return xml grammar with meta element with jsgf name
     */
    public String getByJsgfName(String name) {
        //ignore two grammars with same name
        return executeParamXQuery("head(/*:grammar/*:meta[@name='Source'][@content=$param1]/..)", name + ".jsgf");
    }

}
