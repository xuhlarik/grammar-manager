package cz.muni.fi.xuhlarik.grammarManager.api;

import cz.muni.fi.xuhlarik.grammarManager.FileType;
import cz.muni.fi.xuhlarik.grammarManager.service.FileService;
import cz.muni.fi.xuhlarik.grammarManager.service.GrammarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

/**
 * class of function that are  mapped to requests starting with /api/grammars
 *
 * @author Jakub Uhlarik
 */
@RestController
@CrossOrigin
@RequestMapping(path = "api/grammars")
public class GrammarController {


    private final GrammarService grammarService;


    @Autowired
    public GrammarController(GrammarService grammarService, FileService fileService) {
        this.grammarService = grammarService;

    }

    /**
     * function add file
     *
     * @param file   file to be added
     * @param format format of file added u or null means unknown
     * @return response entity with uid in body of  entity.
     */
    @PostMapping("")
    public ResponseEntity<String> grammarFromFile(@RequestBody String file, @RequestParam(required = false) String format) {
        if (format == null) {
            String uid = grammarService.addGrammarFromFile(file, null);
            return ResponseEntity.status(HttpStatus.CREATED).contentType(MediaType.TEXT_PLAIN).body(uid);
        }
        FileType fileType;
        switch (format.toLowerCase()) {
            case "jsgf" -> fileType = FileType.JSGF;

            case "xml" -> fileType = FileType.XML_SRGS;
            case "u" -> fileType = null;
            default -> throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "invalid file format");
        }
        String uid = grammarService.addGrammarFromFile(file, fileType);
        return ResponseEntity.status(HttpStatus.CREATED).contentType(MediaType.TEXT_PLAIN).body(uid);
    }


    /**
     * function returns grammar with root symbol
     *
     * @param root selected root symbol
     * @return response entity with grammars as root in place
     */
    @GetMapping("/root/{root}")
    public ResponseEntity<String> getGrammarXmlByRoot(@PathVariable String root) {
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_XML).body(grammarService.getGrammarByRootSymbol(root));
    }

    /**
     * function finds jsgf grammar with name
     *
     * @param name name of grammar
     * @return response entity with jsgf grammar as body
     */
    @GetMapping("name/{name}")
    public ResponseEntity<String> getGrammarByJsgfName(@PathVariable String name) {
        return ResponseEntity.ok().contentType(MediaType.TEXT_PLAIN).body(grammarService.getGrammarByJsgfName(name));
    }

    /**
     * function finds grammar with uid and format
     *
     * @param uid    requested uid of grammar
     * @param format requested format of grammar
     * @return response entity with grammar with uid in format as body
     */
    @GetMapping("/{uid}")
    public ResponseEntity<String> getGrammar(@PathVariable String uid, @RequestParam(defaultValue = "xml") String format) {
        FileType typeOfFile;
        switch (format.toLowerCase()) {
            case "xml" -> typeOfFile = FileType.XML_SRGS;
            case "jsgf" -> typeOfFile = FileType.JSGF;
            default -> throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "invalid file format");
        }
        String result = grammarService.getGrammarByUid(uid, typeOfFile);
        return ResponseEntity.ok().contentType(typeOfFile == FileType.XML_SRGS ? MediaType.APPLICATION_XML : MediaType.TEXT_PLAIN).body(result);
    }

    /**
     * function returns all uids used in system
     *
     * @return response entity with as list of uids in json as body
     */
    @GetMapping("/uid")
    public ResponseEntity<String> getAllUid() {
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(grammarService.getAllUid());
    }

}
