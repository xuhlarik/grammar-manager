package cz.muni.fi.xuhlarik.grammarManager;

/**
 * @author Jakub Uhlarik
 */
public enum FileType {
    JSGF,
    XML_SRGS
}
