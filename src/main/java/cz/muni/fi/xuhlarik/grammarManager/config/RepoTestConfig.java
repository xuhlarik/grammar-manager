package cz.muni.fi.xuhlarik.grammarManager.config;

import cz.muni.fi.xuhlarik.grammarManager.dao.XmlDataRepo;
import org.basex.api.client.ClientSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.io.IOException;

/**
 * test configuration class that sets testDB as database name
 *
 * @author Jakub Uhlarik
 */

@Configuration
public class RepoTestConfig {

    @Bean
    @Profile("test")
    @Autowired
    public XmlDataRepo testConfig(ClientSession session) throws IOException {
        return new XmlDataRepo(session, "testDB");
    }
}
