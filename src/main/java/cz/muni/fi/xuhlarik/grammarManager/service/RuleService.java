package cz.muni.fi.xuhlarik.grammarManager.service;

import cz.muni.fi.xuhlarik.grammarManager.FileType;


import cz.muni.fi.xuhlarik.grammarManager.dao.XmlDataRepo;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;


/**
 * function for validation that rule was found
 *
 * @author Jakub Uhlarik
 */
@Service
public class RuleService {


    private final XmlDataRepo xmlDataRepo;
    private final FileService fileService;

    @Autowired
    public RuleService(XmlDataRepo xmlDataRepo, FileService fileService) {
        this.xmlDataRepo = xmlDataRepo;
        this.fileService = fileService;
    }


    /**
     * get file with left symbol and format
     *
     * @param left   left symbol from file
     * @param format format of file
     * @return file with left symbol and format
     */
    public String getRuleByLeft(String left, FileType format) {
        String xml = xmlDataRepo.getRuleByLeft(left);
        if (xml.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "rule with left side: " + left + " not found");
        }
        if (format == FileType.XML_SRGS) {
            return xml;
        }
        return fileService.xmlToJSGF(xml, false);
    }
}
