
FROM maven:3.9.1-eclipse-temurin-17 as buildStage
COPY src src
COPY pom.xml pom.xml
RUN  mvn clean install -DskipTests


FROM openjdk:21
COPY --from=buildStage target/grammarManager-0.1.jar ./
COPY xsd_schema xsd_schema
ENTRYPOINT ["java", "-jar","grammarManager-0.1.jar"]


